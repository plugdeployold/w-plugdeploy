<div class="grid grid-cols-12 gap-4">
                    <div class="col-span-6">
                        <div
                            class="mt-10 mx-auto max-w-7xl px-4 sm:mt-12 sm:px-6 md:mt-16 lg:mt-20 lg:px-8 xl:mt-28"
                        >
                            <div class="sm:text-center lg:text-left">
                                <h1
                                    class="text-4xl tracking-tight font-extrabold text-gray-900 sm:text-5xl md:text-6xl"
                                >
                                    <span class="block text-white xl:inline"
                                        >{{ $mainTitle }} </span
                                    >
                                </h1>
                                <p
                                    class="mt-3 text-base text-white sm:mt-5 sm:text-lg sm:max-w-xl sm:mx-auto md:mt-5 md:text-xl lg:mx-0"
                                >
                                {{ $mainSubTitle }} 
                                </p>
                                <div
                                    class="mt-5 sm:mt-8 sm:flex sm:justify-center lg:justify-start"
                                >
                                    <div class="rounded-md shadow">
                                        <a
                                            href="#"
                                            class="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-blue bg-white hover:bg-indigo-700 md:py-4 md:text-lg md:px-10"
                                        >
                                            Comming Soon
                                        </a>
                                    </div>
                                    <!--<div class="mt-3 sm:mt-0 sm:ml-3">
                                        <a
                                            href="#"
                                            class="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-indigo-700 bg-indigo-100 hover:bg-indigo-200 md:py-4 md:text-lg md:px-10"
                                        >
                                            Try demo
                                        </a>
                                    </div>
                                    <div class="mt-3 sm:mt-0 sm:ml-3">
                                        <a
                                            href="#"
                                            class="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-indigo-700 bg-indigo-100 hover:bg-indigo-200 md:py-4 md:text-lg md:px-10"
                                        >
                                            Live demo
                                        </a>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-span-6">
                        <img
                            srcset="
                                https://firebase.google.com/images/homepage/hero-illo_2x.png 2x,
                                https://firebase.google.com/images/homepage/hero-illo_1x.png 1x
                            "
                            src="/images/homepage/hero-illo_1x.png"
                            alt="Pessoas ajudando umas às outras em um projeto"
                        />
                    </div>
                </div>