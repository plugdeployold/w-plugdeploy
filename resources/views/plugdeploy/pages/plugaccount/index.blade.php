<!DOCTYPE html>

<html lang="en" dir="ltr">
    <head>
        <!--<link rel="stylesheet" href="{{ asset('css/app.css') }}">-->
        <link
            href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css"
            rel="stylesheet"
        />
    </head>
    <body style="background-color: #1abc9c">
        {{-- Start Header Section --}}
        @include('plugdeploy.header.header')
        {{-- End Header Section --}}


        <main class="container mx-auto">

                {{-- Start Hero Section --}}
                @include('plugdeploy.pages.plugaccount.sections.hero')
                {{-- End Hero Section --}}

        </main>

        @include('plugdeploy.components.footer')
    </body>
</html>
