
@include('plugdeploy.components.hero', 
    [
        'mainTitle' => "PlugCommerce helps you build and run successful applications", 
        'mainSubTitle' => "Backed by Plug With Us and loved by app development teams - from startups to globalenterprises"
    ]
)
