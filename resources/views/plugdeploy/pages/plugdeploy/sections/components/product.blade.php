<div
                class="rounded-2xl shadow-xl mb-10"
                style="background-color: #f8f9fa"
            >
                <div class="flex justify-center mt-10 mb-10">
                    <img
                    srcset="{{ $IconSrcSet }}"
                    />
                </div>

                <h1 style="font-size: 25px; color: #424242; font-weight: 500">
                    {{ $ProductName }}
                </h1>
                <div class="mt-5 pl-5 pr-5">
                    <p style="color: #5f6368">
                        {{ $ProductDescription }}
                    </p>
                </div>
                <div class="mt-10 mb-10">
                    <a
                        href="{{ $Product_CTA_href }}"
                        class="bg-gray-900 text-white px-3 py-2 rounded-md text-sm font-medium"
                        >{{ $ProductCTADescription }}</a
                    >
                </div>
            </div>