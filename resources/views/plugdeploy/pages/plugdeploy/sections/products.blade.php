<div class="flex justify-center mb-10">
    <div class="w-10/12 p-20 bg-white text-center rounded-2xl pr-16 shadow-xl">
        <h1
            class="font-semibold text-2xl"
            style="
                font-size: 44px;
                line-height: 54px;
                font-weight: 500;
                color: #424242;
            "
        >
            Products and solutions you can rely on through your app's journey
        </h1>
        <h2
            class="mt-5"
            style="
                font-size: 20px;
                line-height: 54px;
                font-weight: 500;
                color: #5f6368;
            "
        >
            Install pre-packaged, open-source bundles of code to automate common
            development tasks
        </h2>
        <div class="grid grid-cols-3 gap-4 mt-10">
            @include('plugdeploy.pages.plugdeploy.sections.components.product', 
                [
                    'IconSrcSet' => "https://firebase.google.com/images/homepage/home-icon-build.png",
                    'ProductName' => "PlugCommerce", 
                    'ProductDescription' => "Accelerate app development with fully managed backend infrastructure",
                    'ProductCTADescription' => "See more",
                    'Product_CTA_href' => "plugcommerce",
                ]
            )
            @include('plugdeploy.pages.plugdeploy.sections.components.product', 
                [
                    'IconSrcSet' => "https://firebase.google.com/images/homepage/home-icon-release.png",
                    'ProductName' => "PlugEat", 
                    'ProductDescription' => "Accelerate app development with fully managed backend infrastructure",
                    'ProductCTADescription' => "See more",
                    'Product_CTA_href' => "plugeat",
                ]
            )
            @include('plugdeploy.pages.plugdeploy.sections.components.product', 
                [
                    'IconSrcSet' => "https://firebase.google.com/images/homepage/home-icon-engage.png",
                    'ProductName' => "PlugWine", 
                    'ProductDescription' => "Accelerate app development with fully managed backend infrastructure",
                    'ProductCTADescription' => "See more",
                    'Product_CTA_href' => "plugwine",
                ]
            )
            @include('plugdeploy.pages.plugdeploy.sections.components.product', 
                [
                    'IconSrcSet' => "https://firebase.google.com/images/homepage/home-icon-engage.png",
                    'ProductName' => "PlugMonitor", 
                    'ProductDescription' => "Accelerate app development with fully managed backend infrastructure",
                    'ProductCTADescription' => "See more",
                    'Product_CTA_href' => "plugmonitor",
                ]
            )
            @include('plugdeploy.pages.plugdeploy.sections.components.product', 
                [
                    'IconSrcSet' => "https://firebase.google.com/images/homepage/home-icon-build.png",
                    'ProductName' => "PlugProject", 
                    'ProductDescription' => "Accelerate app development with fully managed backend infrastructure",
                    'ProductCTADescription' => "See more",
                    'Product_CTA_href' => "plugproject",
                ]
            )
            @include('plugdeploy.pages.plugdeploy.sections.components.product', 
                [
                    'IconSrcSet' => "https://firebase.google.com/images/homepage/home-icon-release.png",
                    'ProductName' => "PlugAccount", 
                    'ProductDescription' => "Accelerate app development with fully managed backend infrastructure",
                    'ProductCTADescription' => "See more",
                    'Product_CTA_href' => "plugaccount",
                ]
            )
        </div>
    </div>
</div>
