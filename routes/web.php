<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('plugdeploy.pages.plugdeploy.index');
});

Route::get('/plugcommerce', function () {
    return view('plugdeploy.pages.plugcommerce.index');
});

Route::get('/plugeat', function () {
    return view('plugdeploy.pages.plugeat.index');
});

Route::get('/plugwine', function () {
    return view('plugdeploy.pages.plugwine.index');
});

Route::get('/plugproject', function () {
    return view('plugdeploy.pages.plugproject.index');
});

Route::get('/plugaccount', function () {
    return view('plugdeploy.pages.plugaccount.index');
});

Route::get('/plugmonitor', function () {
    return view('plugdeploy.pages.plugmonitor.index');
});
